# 差异报告填写说明

1. 每一个版本的差异放在一个文件夹下，文件夹以版本号命名；
2. 文件夹内放置子系统自己填写的md文件，一个版本里，每个子系统的变更放在一个md文件里，md文件以子系统名字命名，子系统名字请参照表1；
3. 填写API变更时，请填写完整的API，填写示例请参照表2。

**表1** md文件命名对照表

| 子系统           |
| ---------------- |
| ACE开发框架      |
| 包管理           |
| 图形图像         |
| 窗口管理         |
| 事件通知         |
| 基础通信         |
| 位置服务         |
| 多模输入         |
| 电源服务         |
| OS媒体软件       |
| 电话服务         |
| 全球化           |
| 定制             |
| 无障碍软件服务   |
| 资源调度         |
| 公共基础类库     |
| DFX              |
| 升级服务         |
| 分布式硬件       |
| 安全基础能力     |
| 账号             |
| 用户IAM          |
| Misc软件服务     |
| 文件管理         |
| USB服务          |
| 泛sensor服务     |
| 启动服务         |
| 分布式数据管理   |
| 元能力           |
| 网络             |
| 应用             |
| 综合传感处理平台 |
| 测试框架         |

**表2** 填写模板

| 旧版本                                                | 新版本                                                       | 文件                |
| ----------------------------------------------------- | ------------------------------------------------------------ | ------------------- |
| delete(predicates: RdbPredicatesV9): Promise<number>; | delete(table: string, predicates: dataSharePredicates.DataSharePredicates): Promise<number>; | @ohos.data.rdb.d.ts |

